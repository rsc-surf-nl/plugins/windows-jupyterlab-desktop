  
$LOGFILE = "c:\logs\plugin-windows-jupyterlab-desktop.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start windows-jupyterlab-desktop"


  Write-Log "Install jupyterlab desktop"

  $JupyterLabRoot = "c:/jupyterlab"
  $python_env_folder = "/pythonenv"
  $python_env_path = "c:$python_env_folder"
  New-Item -Path $JupyterLabRoot -ItemType Directory


  $url = "https://github.com/jupyterlab/jupyterlab-desktop/releases/download/v4.0.11-1/JupyterLab-Setup-Windows.exe"
  $outpath = "$JupyterLabRoot/JupyterLab-Setup-Windows.exe"
  $wc = New-Object System.Net.WebClient
  $wc.DownloadFile($url, $outpath)

  
  $args = @("/S", "/AllUsers")

  Start-Process -Filepath "$JupyterLabRoot/JupyterLab-Setup-Windows.exe" -ArgumentList $args -Wait

  Remove-Item -Path "$JupyterLabRoot/JupyterLab-Setup-Windows.exe" -Force

  Write-Log "jupyterlab env install"
  New-Item -Path $python_env_path  -ItemType Directory -Force -Wait

  $argsjlab = @("env", "install", "--path $python_env_path", "--force")

  Start-Process -Filepath "$JupyterLabRoot/jupyterlab.exe" -ArgumentList $argsjlab -Wait


  [Environment]::SetEnvironmentVariable(
    "Path",
    [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::Machine) + ";$python_env_path",
    [EnvironmentVariableTarget]::Machine)

  [Environment]::SetEnvironmentVariable(
    "Path",
    [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::Machine) + ";$python_env_path\Scripts",
    [EnvironmentVariableTarget]::Machine)

  Write-Log "Create default settings file"

  $juplab_conf = "C:\Users\Default\AppData\Roaming\jupyterlab-desktop\settings.json"

  New-Item -ItemType File -Path $juplab_conf -Force
 
  Clear-Content $juplab_conf

  Add-Content $juplab_conf '{'
  Add-Content $juplab_conf '"checkForUpdatesAutomatically": false,'
  Add-Content $juplab_conf '"installUpdatesAutomatically": false,'
  Add-Content $juplab_conf '"pythonPath": "\\pythonenv\\python.exe",'
  Add-Content $juplab_conf '"serverEnvVars": {}'
  Add-Content $juplab_conf '}'

}

Main     
 
